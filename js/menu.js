/**
 * @author Steven Knibbs <steven.knibbs@mobas.com>
 */
/*
 * Mobile Main Navigation
 */

jQuery(document).ready(function () {

	/*
	 * Open the menu area if open button is clicked.
	 */
	jQuery('.menu-link').on('click', function (e) {
		e.stopPropagation();
		e.preventDefault();
		jQuery(this).siblings('nav').toggleClass('active');
		return false;
	});

	/**
	 * Traverse back to previous menu if go back button is clicked.
	 */
	jQuery('.nav-container .go-back').on('click', function (e) {
		e.stopImmediatePropagation();
		e.preventDefault();
		jQuery(this).parent('ul').toggleClass('is-hidden');
		return false;
	});

	/**
	 * Close the menu area if close button is clicked.
	 */
	jQuery('.nav-container .close').on('click', function (e) {
		e.stopImmediatePropagation();
		e.preventDefault();
		jQuery(this).parents('nav').toggleClass('active');
		return false;
	});
});

jQuery(window).on('load', function () {
	jQuery(window).on('resize', function () {
		var window = jQuery(this);
		jQuery('.nav-container').each(function() {
			var breakpoint = jQuery(this).find('nav').data('breakpoint');
			var parents = jQuery(this).find('nav ul .has-children');
			parents.children('ul').addClass('is-hidden');

			// Make sure the menu is not in active state on larger devices.
			if (window.width() > breakpoint) {
				jQuery(this).find('nav').removeClass('active');
			}

			if (window.width() > breakpoint && jQuery('html').hasClass('no-touch')) {
				parents.children('a').unbind('click');
				parents.hover(function () {
					jQuery(this).children('ul').removeClass('is-hidden');
				}, function () {
					jQuery(this).children('ul').addClass('is-hidden');
				});
			} else {
				parents.unbind('mouseenter mouseleave');
				parents.children('a').on('click', function (e) {
					e.stopImmediatePropagation();
					e.preventDefault();
					if (jQuery(window).width() > breakpoint) {
						jQuery(this).parent('li').siblings('.has-children').children('ul').addClass('is-hidden');
					}
					jQuery(this).siblings('ul').toggleClass('is-hidden');
					return false;
				});
			}
		});

	}).resize();
});