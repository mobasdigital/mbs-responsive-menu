module.exports = {
    options: {
        watchTask: true,
        server: {
            baseDir: './'
        }
    },
    files: {
        src: [
            'less/*.less',
	        'css/*.css',
	        'html/*.html'
        ]
    }
};