module.exports = {
	all: {
		files: {
			'css/main.css': [
				'less/main.less'
			]
		},
		options: {
			sourceMap: true
		}
	}
};